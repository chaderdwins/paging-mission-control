import json
import datetime
import time

#I wanted to do this one just for fun, I tailored it to fit the given data set.

def time_compare(li):
    #comparing times
    time_list = [item[0].split()[1] for item in li]
    difference = []
    for i in time_list:
        (h, m, s) = i.split(':')
        d = datetime.timedelta(hours=int(h), minutes=int(m), seconds=float(s))
        difference.append(d)
    delta = max(difference) - min(difference)
    return delta < datetime.timedelta(minutes=5)

#parsing the time string for proper json output
def parseTime(t):
    return t[:4] + '-' + t[4:6] + '-' + t[6:8] + 'T' + t[9:-1] + 'Z'

#getting high temperature readings
def getTemp(li):
    temp = []
    for item in li:
        if item[7] == 'TSTAT':
            temp.append(item)
    temp.sort()
    over_red = []
    for i in range(len(temp)):
        if float(temp[i][6]) > float(temp[i][2]):
            over_red.append(temp[i])
    return over_red

#determine if the battery is low
def getBatt(li):
    batt = []
    for item in li:
        if item[7] == 'BATT':
            batt.append(item)
    batt.sort()
    under_red = []
    for i in range(len(batt)):
        if float(batt[i][6]) < float(batt[i][5]):
            under_red.append(batt[i])
    return under_red

#pulling data from the given file
def getFile():
    with open('file.txt', 'r') as f:
        return [line.rstrip().split('|') for line in f]

#main program logic
if __name__ == "__main__":
    current = int(round(time.time() * 1000))
    li = getFile()
    over_red = getTemp(li)
    under_red = getBatt(li)
    print('Welcome to Paging Mission Control\n')
    output = []
    #this is limited to the given data set, with more time I would like to expand and loop over every possible satellite
    #not just the ones for the given data set
    if time_compare(over_red):
        output.append({
            'satelliteId': over_red[0][1],
            'severity': "RED HIGH",
            'component': "TSTAT",
            'timestamp': parseTime(over_red[0][0])
        })
    if time_compare(under_red):
        output.append({
            'satelliteId': under_red[0][1],
            'severity': "RED LOW",
            'component': "BATT",
            'timestamp': parseTime(under_red[0][0])
        })
    #output to runtime to screen
    program_runtime = int(round(time.time() * 1000)) - current
    print(f'Runtime: {program_runtime} ms\n')
    print(json.dumps(output))
